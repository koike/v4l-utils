#!/bin/env python

# SPDX-License-Identifier: GPL-2.0-only
# Copyright 2020 Collabora

import argparse
import enum
import inspect
import logging
import re
import subprocess
import sys
import traceback
import os

BUS_INFO = "platform:rkisp1"
MODULE = "rockchip_isp1"
# Filled by argparse in main
SENSOR = None
OUTPUT_FOLDER = None
STORE_STREAM = None

# --------------------------------------------------------
# Generic v4l2 functions
# --------------------------------------------------------


class EntityFormatError(Exception):
    pass


def run(cmd):
    logging.debug(" ".join(['"{0}"'.format(x) for x in cmd]))
    ret = subprocess.run(cmd, capture_output=True, check=True, timeout=10)
    return ret.stdout.decode('ascii')


# v4l2-ctl -z platform:rkisp1 -d rkisp1_isp --get-subdev-fmt
# media-ctl -d "platform:rkisp1" --get-v4l2 '"rkisp1_isp":0'
def get_subdev_fmt(entity, pad):
    fmt = run(["media-ctl", "-d", BUS_INFO, "--get-v4l2",
              '"{entity}":{pad}'.format(entity=entity, pad=pad)])
    mbus = re.search("fmt:(.*?)/", fmt)
    size = re.findall("(\d+)x(\d+)", fmt)
    offset = re.findall("\((\d+),(\d+)\)", fmt)
    ret = {
        "mbus": mbus.group(1),
        "size": tuple(map(int, size[0])),
    }
    if offset:
        ret["crop_bounds"] = (tuple(map(int, offset[0]))
                              + tuple(map(int, size[1])))
        ret["crop"] = tuple(map(int, offset[1])) + tuple(map(int, size[2]))

    return ret


def set_subdev_fmt(entity, pad, fmt):
    current_fmt = get_subdev_fmt(entity, pad)
    properties = ""

    if "mbus" in fmt and "size" not in fmt:
        fmt["size"] = current_fmt["size"]
    if "size" in fmt and "mbus" not in fmt:
        fmt["mbus"] = current_fmt["mbus"]

    if ("size" in fmt):
        properties = "fmt:{mbus}/{width}x{height}"
        properties = properties.format(mbus=fmt["mbus"],
                                       width=fmt["size"][0],
                                       height=fmt["size"][1])
    if ("crop" in fmt):
        crop = " crop: ({left},{top})/{width}x{height}"
        crop = crop.format(left=fmt["crop"][0],
                           top=fmt["crop"][1],
                           width=fmt["crop"][2],
                           height=fmt["crop"][3])
        properties = properties + crop

    run(["media-ctl", "-d", BUS_INFO, "--set-v4l2",
        '"{entity}":{pad} [{properties}]'.format(entity=entity,
                                                 pad=pad,
                                                 properties=properties)])

    current_fmt = get_subdev_fmt(entity, pad)
    if (
            ("size" in fmt and fmt["size"] != current_fmt["size"]) or
            ("mbus" in fmt and fmt["mbus"] != current_fmt["mbus"]) or
            ("crop" in fmt and fmt["crop"] != current_fmt["crop"])
    ):
        error_msg = "{entity}:{pad}: Format couldn't be set. " \
                    "Expected {expected}; Got {got}"
        raise EntityFormatError(error_msg.format(entity=entity,
                                                 pad=pad,
                                                 expected=fmt,
                                                 got=current_fmt))
    return current_fmt


# v4l2-ctl -z platform:rkisp1 -d "rkisp1_mainpath" -V
def get_video_fmt(entity):
    fmt = run(["v4l2-ctl", "-z", BUS_INFO, "-d", str(entity), "-V"])
    size = re.search("(\d+)/(\d+)", fmt)
    pixelformat = re.search("'(.*?)'", fmt)
    return {
        "size": (int(size.group(1)), int(size.group(2))),
        "pixelformat": pixelformat.group(1)
    }


# v4l2-ctl -z platform:rkisp1 -d "rkisp1_mainpath" \
# -v width=1920,height=1440,pixelformat=BA81
def set_video_fmt(entity, fmt):
    properties = ""
    if "size" in fmt:
        size = "width={width},height={height},".format(width=fmt["size"][0],
                                                       height=fmt["size"][1])
        properties = properties + size
    if "pixelformat" in fmt:
        pixfmt = "pixelformat={pixelformat}"
        properties = properties + pixfmt.format(pixelformat=fmt["pixelformat"])

    run(["v4l2-ctl", "-z", BUS_INFO, "-d", str(entity), "-v", properties])

    current_fmt = get_video_fmt(entity)
    if (
        ("size" in fmt and fmt["size"] != current_fmt["size"]) or
        ("pixelformat" in fmt and
            fmt["pixelformat"] != current_fmt["pixelformat"])
    ):
        error_msg = "{entity}: Format couldn't be set. " \
                    "Expected {expected}; Got {got}"
        raise EntityFormatError(error_msg.format(entity=entity,
                                                 expected=fmt,
                                                 got=current_fmt))
    return current_fmt


def disable_all_links():
    run(["media-ctl", "-r"])


# v4l2-ctl -z platform:rkisp1 -d rkisp1_isp --list-subdev-mbus-codes 2
def get_mbus_codes(entity, pad):
    output = run(["v4l2-ctl", "-z", BUS_INFO, "-d", str(entity),
                 "--list-subdev-mbus-codes", str(pad)])
    return re.findall("MEDIA_BUS_FMT_(.+)", output)


# v4l2-ctl --stream-mmap --stream-count=100 -d $CAP_SP_DEV \
# --stream-to=/tmp/test_sp.raw
def start_stream(entity, output_file_path):
    cmd = ["v4l2-ctl", "-z", BUS_INFO, "-d", str(entity), "--stream-mmap",
           "--stream-count", "20"]
    if STORE_STREAM:
        cmd = cmd + ["--stream-to", output_file_path]

    run(cmd)

    if not STORE_STREAM:
        return

    fmt = get_video_fmt(entity)
    msg = "Stream captured to {output_file_path}, with format {fmt}"
    logging.info(msg.format(output_file_path=output_file_path, fmt=fmt))


# --------------------------------------------------------
# rkisp1 specific functions
# --------------------------------------------------------

NUMBER_TEST_SKIPS = 0
NUMBER_TEST_FAILURES = 0
NUMBER_TEST_SUCCESS = 0


class Rkisp1TestSkip(Exception):
    pass


class FmtTypes(enum.IntEnum):
    BAYER = 0
    YUV = 1


class LimDim(enum.IntEnum):
    RSZ_MP_SRC_MAX_WIDTH = 4416
    RSZ_MP_SRC_MAX_HEIGHT = 3312
    RSZ_SP_SRC_MAX_WIDTH = 1920
    RSZ_SP_SRC_MAX_HEIGHT = 1920
    RSZ_SRC_MIN_WIDTH = 32
    RSZ_SRC_MIN_HEIGHT = 16


class Link(enum.IntEnum):
    ENABLED = 1
    ENABLE = 1
    DISABLED = 0
    DISABLE = 0


class Entities(enum.Enum):
    isp = "rkisp1_isp"
    resizer_mp = "rkisp1_resizer_mainpath"
    resizer_sp = "rkisp1_resizer_selfpath"
    cap_mp = "rkisp1_mainpath"
    cap_sp = "rkisp1_selfpath"

    def __str__(self):
        return str(self.value)


class IspPads(enum.IntEnum):
    SINK_VIDEO = 0,
    SINK_PARAMS = 1,
    SOURCE_VIDEO = 2,
    SOURCE_STATS = 3,

    def __str__(self):
        return str(self.value)


class ResizerPads(enum.IntEnum):
    SINK = 0,
    SOURCE = 1,


# TODO: transform this to a generic function that gets the source pad too
def rkisp1_get_link_status_to_sink(sink_entity, sink_pad):
    topology = run(["media-ctl", "-d", BUS_INFO, "-p"])
    pattern = '"{sink_entity}":{sink_pad} \[(.+?)\]'
    pattern = pattern.format(sink_entity=sink_entity, sink_pad=sink_pad)
    status = re.findall(pattern, topology)
    if ('ENABLED' in status):
        return Link.ENABLED
    else:
        return Link.DISABLED


# $mediactl -l "'$sensor':0 -> 'rkisp1_isp':0 [0]"
def rkisp1_disable_link(src_entity, src_pad, sink_entity, sink_pad):
    link = "'{src_entity}':{src_pad} -> '{sink_entity}':{sink_pad} [0]"
    link = link.format(src_entity=src_entity,
                       src_pad=src_pad,
                       sink_entity=sink_entity,
                       sink_pad=sink_pad)
    run(["media-ctl", "-d", BUS_INFO, "-l", link])
    if rkisp1_get_link_status_to_sink(sink_entity, sink_pad) != Link.DISABLED:
        msg = "Couldn't disable link {link}".format(link=link)
        raise EntityFormatError(msg)


# $mediactl -l "'$sensor':0 -> 'rkisp1_isp':0 [1]"
def rkisp1_enable_link(src_entity, src_pad, sink_entity, sink_pad):
    link = "'{src_entity}':{src_pad} -> '{sink_entity}':{sink_pad} [1]"
    link = link.format(src_entity=src_entity,
                       src_pad=src_pad,
                       sink_entity=sink_entity,
                       sink_pad=sink_pad)
    run(["media-ctl", "-d", BUS_INFO, "-l", link])
    if rkisp1_get_link_status_to_sink(sink_entity, sink_pad) != Link.ENABLED:
        msg = "Couldn't enalble link {link}".format(link=link)
        raise EntityFormatError(msg)


def rkisp1_propagate_isp_src_fmt(fmt):
    if (
            rkisp1_get_link_status_to_sink(Entities.resizer_mp, 0)
            == Link.ENABLED
    ):
        fmt_mainpath = set_subdev_fmt(Entities.resizer_mp,
                                      ResizerPads.SINK, fmt)
        del(fmt_mainpath["crop"])
        fmt_mainpath = set_subdev_fmt(Entities.resizer_mp,
                                      ResizerPads.SOURCE, fmt_mainpath)
        set_video_fmt(Entities.cap_mp, fmt_mainpath)

    if (
            rkisp1_get_link_status_to_sink(Entities.resizer_sp, 0)
            == Link.ENABLED
    ):
        fmt_selfpath = set_subdev_fmt(Entities.resizer_sp,
                                      ResizerPads.SINK, fmt)
        del(fmt_selfpath["crop"])
        fmt_selfpath = set_subdev_fmt(Entities.resizer_sp,
                                      ResizerPads.SOURCE, fmt_selfpath)
        set_video_fmt(Entities.cap_sp, fmt_selfpath)


def rkisp1_propagate_isp_sink_fmt(fmt):
    fmt = set_subdev_fmt(Entities.isp, IspPads.SOURCE_VIDEO, fmt)
    rkisp1_propagate_isp_src_fmt(fmt)


def rkisp1_propagate_sensor_fmt(isp_source_mbus=None):
    fmt = get_subdev_fmt(SENSOR, 0)
    fmt["crop"] = (0, 0) + fmt["size"]
    fmt = set_subdev_fmt(Entities.isp, IspPads.SINK_VIDEO, fmt)
    if isp_source_mbus:
        fmt["mbus"] = isp_source_mbus
    rkisp1_propagate_isp_sink_fmt(fmt)


def rkisp1_test_decorator(function):
    def wrapper():
        global NUMBER_TEST_FAILURES
        global NUMBER_TEST_SUCCESS
        global NUMBER_TEST_SKIPS
        try:
            logging.info("")
            logging.info("Starting test {test}".format(test=function.__name__))
            function()
        except Rkisp1TestSkip as e:
            logging.debug(traceback.format_exc())
            logging.info("Test {test}: Skipped".format(test=function.__name__))
            NUMBER_TEST_SKIPS = NUMBER_TEST_SKIPS + 1
        except Exception as e:
            logging.debug(traceback.format_exc())
            logging.info("Test {test}: Failed".format(test=function.__name__))
            NUMBER_TEST_FAILURES = NUMBER_TEST_FAILURES + 1
        else:
            logging.info("Test {test}: Passed".format(test=function.__name__))
            NUMBER_TEST_SUCCESS = NUMBER_TEST_SUCCESS + 1
    return wrapper


def rkisp1_get_func_name():
    return inspect.stack()[1].function


# Runs all functions that starts with prefix "rkisp1_test_"
def run_tests(test):
    for key, value in globals().items():
        if callable(value) and key.startswith(test):
            value()
    logging.info("")
    logging.info("------------------------------------------------------")
    msg = "Summary: Total: " \
          "{total}, Failures: {failures}, Success: {success}, Skipped: {skips}"
    logging.info(msg.format(total=NUMBER_TEST_FAILURES + NUMBER_TEST_SUCCESS,
                 failures=NUMBER_TEST_FAILURES,
                 success=NUMBER_TEST_SUCCESS,
                 skips=NUMBER_TEST_SKIPS))
    logging.info("------------------------------------------------------")


# Put the driver in a known state
def rkisp1_prepare_test():
    # try:
    #     run(["rmmod", MODULE])
    # except subprocess.CalledProcessError:
    #     pass
    # run(["modprobe", MODULE])
    disable_all_links()
    rkisp1_enable_link(SENSOR, 0, Entities.isp, IspPads.SINK_VIDEO)
    rkisp1_enable_link(Entities.isp, IspPads.SOURCE_VIDEO,
                       Entities.resizer_mp, ResizerPads.SINK)
    rkisp1_enable_link(Entities.isp, IspPads.SOURCE_VIDEO,
                       Entities.resizer_sp, ResizerPads.SINK)
    rkisp1_enable_link(Entities.resizer_mp, ResizerPads.SOURCE,
                       Entities.cap_mp, 0)
    rkisp1_enable_link(Entities.resizer_sp, ResizerPads.SOURCE,
                       Entities.cap_sp, 0)
    try:
        set_subdev_fmt(SENSOR, 0, {"size": (800, 600)})
    except EntityFormatError as e:
        logging.debug(str(e))
    rkisp1_propagate_sensor_fmt(isp_source_mbus="YUYV8_2X8")
    set_video_fmt(Entities.cap_mp, {"pixelformat": "NV12"})
    set_video_fmt(Entities.cap_sp, {"pixelformat": "NV12"})
    logging.debug("end of {func} --------".format(func=rkisp1_get_func_name()))


# --------------------------------------------------------
# rkisp1 tests
# --------------------------------------------------------

CAP_FORMAT_TYPES = {
    "YUYV": FmtTypes.YUV,    # (YUYV 4:2:2)
    "YVYU": FmtTypes.YUV,    # (YVYU 4:2:2)
    "VYUY": FmtTypes.YUV,    # (VYUY 4:2:2)
    "422P": FmtTypes.YUV,    # (Planar YUV 4:2:2)
    "NV16": FmtTypes.YUV,    # (Y/CbCr 4:2:2)
    "NV61": FmtTypes.YUV,    # (Y/CrCb 4:2:2)
    "YM61": FmtTypes.YUV,    # (Planar YVU 4:2:2 (N-C))
    "NV21": FmtTypes.YUV,    # (Y/CrCb 4:2:0)
    "NV12": FmtTypes.YUV,    # (Y/CbCr 4:2:0)
    "NM21": FmtTypes.YUV,    # (Y/CrCb 4:2:0 (N-C))
    "NM12": FmtTypes.YUV,    # (Y/CbCr 4:2:0 (N-C))
    "YU12": FmtTypes.YUV,    # (Planar YUV 4:2:0)
    "YV12": FmtTypes.YUV,    # (Planar YVU 4:2:0)
    "YM24": FmtTypes.YUV,    # (Planar YUV 4:4:4 (N-C))
    "GREY": FmtTypes.YUV,    # (8-bit Greyscale)
    "RGGB": FmtTypes.BAYER,  # (8-bit Bayer RGRG/GBGB)
    "GRBG": FmtTypes.BAYER,  # (8-bit Bayer GRGR/BGBG)
    "GBRG": FmtTypes.BAYER,  # (8-bit Bayer GBGB/RGRG)
    "BA81": FmtTypes.BAYER,  # (8-bit Bayer BGBG/GRGR)
    "RG10": FmtTypes.BAYER,  # (10-bit Bayer RGRG/GBGB)
    "BA10": FmtTypes.BAYER,  # (10-bit Bayer GRGR/BGBG)
    "GB10": FmtTypes.BAYER,  # (10-bit Bayer GBGB/RGRG)
    "BG10": FmtTypes.BAYER,  # (10-bit Bayer BGBG/GRGR)
    "RG12": FmtTypes.BAYER,  # (12-bit Bayer RGRG/GBGB)
    "BA12": FmtTypes.BAYER,  # (12-bit Bayer GRGR/BGBG)
    "GB12": FmtTypes.BAYER,  # (12-bit Bayer GBGB/RGRG)
    "BG12": FmtTypes.BAYER,  # (12-bit Bayer BGBG/GRGR)
}

ISP_FORMAT_TYPES = {
    "YUYV8_2X8": FmtTypes.YUV,
    "SRGGB10_1X10": FmtTypes.BAYER,
    "SBGGR10_1X10": FmtTypes.BAYER,
    "SGBRG10_1X10": FmtTypes.BAYER,
    "SGRBG10_1X10": FmtTypes.BAYER,
    "SRGGB12_1X12": FmtTypes.BAYER,
    "SBGGR12_1X12": FmtTypes.BAYER,
    "SGBRG12_1X12": FmtTypes.BAYER,
    "SGRBG12_1X12": FmtTypes.BAYER,
    "SRGGB8_1X8": FmtTypes.BAYER,
    "SBGGR8_1X8": FmtTypes.BAYER,
    "SGBRG8_1X8": FmtTypes.BAYER,
    "SGRBG8_1X8": FmtTypes.BAYER,
    "YUYV8_1X16": FmtTypes.YUV,
    "YVYU8_1X16": FmtTypes.YUV,
    "UYVY8_1X16": FmtTypes.YUV,
    "VYUY8_1X16": FmtTypes.YUV,
}


@rkisp1_test_decorator
def RKISP1_TEST_the_test():
    rkisp1_prepare_test()
    get_subdev_fmt(Entities.isp, 0)
    set_subdev_fmt(Entities.isp, 0, {"mbus": "SRGGB10_1X10",
                   "size": (800, 600), "crop": (0, 0, 800, 600)})
    set_subdev_fmt(Entities.isp, 0, {"size": (300, 300)})
    set_subdev_fmt(Entities.isp, 0, {"crop": (0, 0, 300, 300)})
    set_subdev_fmt(Entities.isp, 0, {"mbus": "SRGGB8_1X8"})
    get_video_fmt(Entities.cap_mp)
    set_video_fmt(Entities.cap_mp, {"size": (200, 200), "pixelformat": "NV12"})
    set_video_fmt(Entities.cap_mp, {"size": (800, 600)})
    set_video_fmt(Entities.cap_mp, {"pixelformat": "YUYV"})
    rkisp1_get_link_status_to_sink(Entities.resizer_mp, 0)
    rkisp1_enable_link(Entities.isp, 2, Entities.resizer_mp, 0)
    get_mbus_codes(Entities.isp, IspPads.SOURCE_VIDEO)


@rkisp1_test_decorator
def RKISP1_TEST_simple_stream():
    rkisp1_prepare_test()
    f = "{out}/{test}_mp.raw".format(out=OUTPUT_FOLDER,
                                     test=rkisp1_get_func_name())
    start_stream(Entities.cap_mp, f)
    f = "{out}/{test}_sp.raw".format(out=OUTPUT_FOLDER,
                                     test=rkisp1_get_func_name())
    start_stream(Entities.cap_sp, f)


@rkisp1_test_decorator
def RKISP1_TEST_stream_all_isp_source_formats_mainpath():
    disable_all_links()
    rkisp1_enable_link(SENSOR, 0, Entities.isp, IspPads.SINK_VIDEO)
    rkisp1_enable_link(Entities.isp, IspPads.SOURCE_VIDEO,
                       Entities.resizer_mp, ResizerPads.SINK)
    rkisp1_enable_link(Entities.resizer_mp, ResizerPads.SOURCE,
                       Entities.cap_mp, 0)

    for mbus in get_mbus_codes(Entities.isp, IspPads.SOURCE_VIDEO):
        logging.debug("{test}: mbus {mbus}".format(test=rkisp1_get_func_name(),
                                                   mbus=mbus))
        rkisp1_propagate_sensor_fmt(isp_source_mbus=mbus)
        if ISP_FORMAT_TYPES[mbus] == FmtTypes.YUV:
            set_video_fmt(Entities.cap_mp, {"pixelformat": "NV12"})
        else:
            set_video_fmt(Entities.cap_mp, {"pixelformat": "RG10"})
        f = "{out}/{test}_{mbus}.raw".format(out=OUTPUT_FOLDER,
                                             test=rkisp1_get_func_name(),
                                             mbus=mbus)
        start_stream(Entities.cap_mp, f)


@rkisp1_test_decorator
def RKISP1_TEST_isp_simple_small_resolution():
    rkisp1_prepare_test()
    try:
        set_subdev_fmt(SENSOR, 0, {"size": (LimDim.RSZ_SRC_MIN_WIDTH,
                                            LimDim.RSZ_SRC_MIN_HEIGHT)})
    except EntityFormatError as e:
        logging.debug(str(e))
        fmt = get_subdev_fmt(SENSOR, 0)
        if (
                fmt["size"][0] < LimDim.RSZ_SRC_MIN_WIDTH or
                fmt["size"][1] < LimDim.RSZ_SRC_MIN_HEIGHT
        ):
            raise Rkisp1TestSkip(str(e))

    rkisp1_propagate_sensor_fmt(isp_source_mbus="YUYV8_2X8")
    f = "{out}/{test}_mp.raw".format(out=OUTPUT_FOLDER,
                                     test=rkisp1_get_func_name())
    start_stream(Entities.cap_mp, f)
    f = "{out}/{test}_sp.raw".format(out=OUTPUT_FOLDER,
                                     test=rkisp1_get_func_name())
    start_stream(Entities.cap_sp, f)


@rkisp1_test_decorator
def RKISP1_TEST_isp_simple_big_resolution_mp():
    rkisp1_prepare_test()
    rkisp1_disable_link(Entities.isp, IspPads.SOURCE_VIDEO,
                        Entities.resizer_sp, ResizerPads.SINK)
    rkisp1_disable_link(Entities.resizer_sp, ResizerPads.SOURCE,
                        Entities.cap_sp, 0)
    try:
        set_subdev_fmt(SENSOR, 0, {"size": (LimDim.RSZ_MP_SRC_MAX_WIDTH,
                                            LimDim.RSZ_MP_SRC_MAX_HEIGHT)})
    except EntityFormatError as e:
        logging.debug(str(e))
        fmt = get_subdev_fmt(SENSOR, 0)
        if (
            fmt["size"][0] > LimDim.RSZ_MP_SRC_MAX_WIDTH or
            fmt["size"][1] > LimDim.RSZ_MP_SRC_MAX_HEIGHT
        ):
            raise Rkisp1TestSkip(str(e))

    rkisp1_propagate_sensor_fmt(isp_source_mbus="YUYV8_2X8")
    f = "{out}/{test}.raw".format(out=OUTPUT_FOLDER,
                                  test=rkisp1_get_func_name())
    start_stream(Entities.cap_mp, f)


@rkisp1_test_decorator
def RKISP1_TEST_isp_simple_big_resolution_sp():
    rkisp1_prepare_test()
    rkisp1_disable_link(Entities.isp, IspPads.SOURCE_VIDEO,
                        Entities.resizer_mp, ResizerPads.SINK)
    rkisp1_disable_link(Entities.resizer_mp, ResizerPads.SOURCE,
                        Entities.cap_mp, 0)
    try:
        set_subdev_fmt(SENSOR, 0, {"size": (LimDim.RSZ_SP_SRC_MAX_WIDTH,
                                            LimDim.RSZ_SP_SRC_MAX_HEIGHT)})
    except EntityFormatError as e:
        logging.debug(str(e))
        fmt = get_subdev_fmt(SENSOR, 0)
        if (
            fmt["size"][0] > LimDim.RSZ_SP_SRC_MAX_WIDTH or
            fmt["size"][1] > LimDim.RSZ_SP_SRC_MAX_HEIGHT
        ):
            raise Rkisp1TestSkip(str(e))

    rkisp1_propagate_sensor_fmt(isp_source_mbus="YUYV8_2X8")
    f = "{out}/{test}.raw".format(out=OUTPUT_FOLDER,
                                  test=rkisp1_get_func_name())
    start_stream(Entities.cap_sp, f)


if __name__ == "__main__":
    formatter = argparse.ArgumentDefaultsHelpFormatter
    parser = argparse.ArgumentParser(formatter_class=formatter)
    parser.add_argument("-t", "--test", help="specific test to be run",
                        default="RKISP1_TEST_")
    parser.add_argument("-v", "--verbose", help="verbose output",
                        action="store_true")
    parser.add_argument("-s", "--sensor", help="select sensor to use in tests",
                        default="ov5647 4-0036")
    parser.add_argument("-o", "--output",
                        help="directory to add output streams", default=".")
    parser.add_argument("-S", "--store", help="store stream to output folder",
                        action="store_true")
    args = parser.parse_args()
    SENSOR = args.sensor
    OUTPUT_FOLDER = args.output
    STORE_STREAM = args.store

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logfile = "{output_folder}/log.txt".format(output_folder=OUTPUT_FOLDER)
    logging.basicConfig(level=level, filename=logfile, filemode='w')

    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(level)

    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    logging.info("Saving logs at " + logfile)
    logging.info("Testing with sensor " + SENSOR)
    logging.info("Output directory " + OUTPUT_FOLDER)

    run_tests(args.test)
